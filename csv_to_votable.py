""" Stellar flux model -- Vacca et al. 1996
Convert CSV datafile to VOTable format with units

Author: Olivier Iffrig <olivier.iffrig@cea.fr>
License: GPLv3
"""

from astropy.io import ascii
import astropy.units as U

T = ascii.read("Vacca96.csv")

# Convert the log to the real flux
T["Q_0"] = 10.**T["log Q_0"]
del T["log Q_0"]

# Add the units
T["M_spec"].unit = U.Msun
T["M_evol"].unit = U.Msun
T["Q_0"].unit = 1 / U.s

T.write("Vacca96.xml", format="votable")

