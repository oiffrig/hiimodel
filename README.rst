
================
HII region model
================

The model is based on a computation of the stellar flux with a fit (by O.
Iffrig) of [Vacca96]_ data. The HII region properties are then computed using
Eqs. (5) and (7) of [Geen15]_.

Contents
========

Scripts
-------

``csv_to_votable.py``
    Convert the CSV data file to VOTable
``hii_model.py``
    Library computing all the parameters from the model
``plot_cmp.py``
    Compare the flux fit to data
``sample_mass.py``
    Show model output as a function of mass
``stat_imf.py``
    Show model output averaged over a power-law IMF

Data
----

``Vacca96.csv``
    CSV copy of tables 5, 6 and 7 of [Vacca96]_
``Vacca96.xml``
    Data file converted to VOTable with units. See ``csv_to_votable.py``.


References
==========

.. [Vacca96] `Vacca et al. 1996`_, ApJ 460, 914
   Stellar flux as a function of mass

   .. _Vacca et al. 1996: http://adsabs.harvard.edu/abs/1996ApJ...460..914V

.. [Geen15] `Geen et al. 2015`_, MNRAS 454, 4484 – 4502
   HII region momentum

   .. _Geen et al. 2015: http://adsabs.harvard.edu/abs/2015MNRAS.454.4484G

License
=======

The code is placed under the terms of the GNU General Public License, v.3 or
higher.

