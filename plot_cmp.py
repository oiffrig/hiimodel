""" Stellar flux model -- Compare fit to data

Data: Vacca et al. 1996
Fit by O. Iffrig

Author: Olivier Iffrig <olivier.iffrig@cea.fr>
License: GPLv3
"""

import astropy.table as T
import astropy.units as U
import matplotlib.pyplot as plt
import numpy as np

# Add all models to compare with the fit
# The models should be in VOTable format with the following columns:
#   Q_0: star flux
#   M_evol: evolutionary mass
# The units are taken from the VOTable
# The file is assumed to be {name}.xml
models = ['Vacca96']

# Fitted coefficients -- O. Iffrig based on Vacca et al. 1996
COEFFS = {
    'y0': 4.3652506381760366e48, # s^-1
    'x0': 27.280988242804312, # Msun
    'a': 6.840015602892084,
    'b': 1.1421666570429914,
    'c': 1.8674625910907161,
}

@np.vectorize
def fit(x):
    """ Fit function -- O. Iffrig based on Vacca et al. 1996

    The fitted coefficients are taken from the COEFFS array
    F = 2**b * y0 * (x / x0)**a / (1 + (x / x0)**((a-c)/b))**b
    F: stellar flux (s^-1)
    x: star mass (Msun)
    """
    a = COEFFS['a']
    b = COEFFS['b']
    c = COEFFS['c']
    x0 = COEFFS['x0']
    y0 = COEFFS['y0']
    return 2**b * y0 * (x / x0)**a / (1 + (x / x0)**((a-c)/b))**b

# Choose units
mass_unit = U.Msun
flux_unit = U.s**(-1)

# Plot data
for model in models:
    t = T.Table.read('{}.xml'.format(model))
    t['M_evol'].convert_unit_to(mass_unit)
    t['Q_0'].convert_unit_to(flux_unit)
    plt.scatter(t['M_evol'], t['Q_0'], marker="+", label=model)

# Plot the fit
fit_m = np.logspace(np.log10(8.), np.log10(150.), 100)
fit_t = fit(fit_m)
plt.loglog(fit_m, fit_t, label='Fit')

plt.xlabel("Mass ({})".format(mass_unit.to_string(format="latex_inline")))
plt.ylabel("Photon flux ({})".format(flux_unit.to_string(format="latex_inline")))
plt.legend(loc='best')

# Compute nicer parametrization
params = [
    ('K', 2**COEFFS['b'] * COEFFS['y0']),
    ('x0', COEFFS['x0']),
    ('a', COEFFS['a']),
    ('b', (COEFFS['a'] - COEFFS['c']) / COEFFS['b']),
    ('c', COEFFS['b']),
]
print "x = M / Msun"
print "q = Q / s**(-1)"
print "q = K * (x / x0)**a / (1 + (x / x0)**b)**c"
for pname, pval in params:
    print "{:2s} = {:.15e}".format(pname, pval)

plt.show()

