""" HII region model -- Compute averages over the IMF

Stellar flux based on a fit by O.Iffrig on data from Vacca et al. 1996
HII region model based on Geen et al. 2015

Plot model outputs (stellar flux, HII region radius, momentum, energy) averaged
over a power-law (Salpeter) IMF.

Author: Olivier Iffrig <olivier.iffrig@cea.fr>
License: GPLv3
"""

import astropy.constants as C
import astropy.units as U

import matplotlib.colors as col
import matplotlib.pyplot as plt
import numpy as np

from hii_model import alpha_B_HII, hii_fb

# Constants
mH = C.m_p.to("g").value # Hydrogen mass in g
pc = C.pc.to("cm").value # pc in cm
Myr = U.Myr.to("s") # Myr in s
k_B = C.k_B.cgs.value # Boltzmann constant in erg / K

# Physical parameters
X = 0.76 # Hydrogen fraction
gamma = 5. / 3. # Adiabatic index
hii_c = 12.5e5 # Sound speed in ionized gas (cm/s)

# IMF parameters
imf_low = 8.0 # Minimal mass (Msun)
imf_hi = 120.0 # Maximal mass (Msun)
imf_index = -2.35 # Exponent (Salpeter IMF)

# Density profile
r_0 = pc # Radius for the given density (cm)
hii_w = 1.1 # Density profile exponent (n(r) = n_0 (r/r_0)**(-w))

# Ambient gas parameters
n_0_arr = np.logspace(-1, 4, 100) # Ambient density factor (cm**(-3))
T_amb = 8000. # Ambient temperature (K, for recombination parameter)

# Other HII region parameters
st_mbins = np.logspace(np.log10(imf_low), np.log10(imf_hi), 1001) # Star mass bins (Msun)
st_m = np.sqrt(st_mbins[1:] * st_mbins[:-1]) # Star mass bin centers (Msun)
hii_t = 4 * Myr # Age of the HII region (s)
T_hii = 0 # HII region temperature (K, for thermal energy injection)

# Computation -------------------------------------------------------------------

hii_alpha = alpha_B_HII(T_amb) # Recombination parameter (cm**3 / s)
hii_param = mH / X * n_0_arr * r_0**hii_w # mu n_0 r_0**w (g cm**(w-3))
hii_mp = mH / X # Particle mass (g)
hii_T2 = k_B / (mH / X) * T_hii # Temperature expressed as P / rho (cm**2 / s**2)

hii_s, hii_r, st_p, st_e = hii_fb(st_m, hii_param, hii_w, hii_c, hii_t, hii_alpha, hii_mp, hii_T2, gamma)

e = imf_index + 1
imf_bin = (st_mbins[1:]**e - st_mbins[:-1]**e) / (imf_hi**e - imf_low**e) # Integral of the IMF in the bins

int_p = np.einsum("ij, i -> j", st_p, imf_bin) # Momentum, averaged over the IMF
int_e = np.einsum("ij, i -> j", st_e, imf_bin) # Energy, averaged over the IMF

# Plot --------------------------------------------------------------------------
plt.figure()
plt.subplot(221)
plt.loglog(st_m, hii_s)
plt.xlabel(r"Star mass ($\mathrm{M_\odot}$)")
plt.ylabel(r"Photon flux ($\mathrm{s^{-1}}$)")

plt.subplot(223)
plt.loglog(st_m, imf_bin / np.diff(st_mbins))
plt.xlabel(r"Star mass ($\mathrm{M_\odot}$)")
plt.ylabel(r"IMF ($\mathrm{M_\odot^{-1}}$)")

plt.subplot(222)
plt.loglog(n_0_arr, int_p)
plt.xlabel(r"Ambient density ($\mathrm{cm^{-3}}$)")
plt.ylabel(r"$\mathrm{H_{II}}$ region momentum ($\mathrm{g\ cm / s}$)")

plt.subplot(224)
plt.loglog(n_0_arr, int_e)
plt.xlabel(r"Ambient density ($\mathrm{cm^{-3}}$)")
plt.ylabel(r"$\mathrm{H_{II}}$ region energy ($\mathrm{erg}$)")

plt.tight_layout()

plt.figure()
plt.pcolormesh(st_mbins, n_0_arr, st_p.T, norm=col.LogNorm())
cb = plt.colorbar()
cs = plt.contour(st_m, n_0_arr, st_p.T, [1e42, 1e43, 1e44, 1e45], colors='k')

plt.loglog()
cb.set_label(r"$\mathrm{H_{II}}$ region momentum ($\mathrm{g\ cm / s}$)")
plt.clabel(cs, inline=True)
plt.xlabel(r"Star mass ($\mathrm{M_\odot}$)")
plt.ylabel(r"Ambient density ($\mathrm{cm^{-3}}$)")
plt.loglog()

plt.show()

