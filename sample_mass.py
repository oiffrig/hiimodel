""" HII region model -- Plot output as a function of mass

Stellar flux based on a fit by O.Iffrig on data from Vacca et al. 1996
HII region model based on Geen et al. 2015

Plot model outputs (stellar flux, HII region radius, momentum, energy) as a
function of mass.

Author: Olivier Iffrig <olivier.iffrig@cea.fr>
License: GPLv3
"""

import astropy.constants as C
import astropy.units as U

import matplotlib.pyplot as plt
import numpy as np

from hii_model import alpha_B_HII, hii_fb

# Constants
mH = C.m_p.to("g").value # Hydrogen mass in g
pc = C.pc.to("cm").value # pc in cm
Myr = U.Myr.to("s") # Myr in s
k_B = C.k_B.cgs.value # Boltzmann constant in erg / K

# Physical parameters
X = 0.76 # Hydrogen fraction
gamma = 5. / 3. # Adiabatic index
hii_c = 12.5e5 # Sound speed in ionized gas (cm/s)

# Density profile
r_0 = pc # Radius for the given density (cm)
hii_w = 1.1 # Density profile exponent (n(r) = n_0 (r/r_0)**(-w))

# Ambient gas parameters
n_0_arr = np.array([1., 10., 100., 1000., 3400.]) # Ambient density factor (cm**(-3))
T_amb = 8000. # Ambient temperature (K, for recombination parameter)

# Other HII region parameters
st_m = np.logspace(np.log10(8.), np.log10(120.), 50) # Star mass (Msun)
hii_t = 4 * Myr # Age of the HII region (s)
T_hii = 0 # HII region temperature (K, for thermal energy injection)

# Computation -------------------------------------------------------------------

hii_alpha = alpha_B_HII(T_amb) # Recombination parameter (cm**3 / s)
hii_param_arr = mH / X * n_0_arr * r_0**hii_w # mu n_0 r_0**w (g cm**(w-3))
hii_mp = mH / X # Particle mass (g)
hii_T2 = k_B / (mH / X) * T_hii # Temperature expressed as P / rho (cm**2 / s**2)

hii_s, hii_r, st_p, st_e = hii_fb(st_m, hii_param_arr, hii_w, hii_c, hii_t, hii_alpha, hii_mp, hii_T2, gamma)

# Plot --------------------------------------------------------------------------
lbl_fmt = "{:g}"

plt.subplot(221)
plt.loglog(st_m, hii_s)
plt.xlabel(r"Star mass ($\mathrm{M_\odot}$)")
plt.ylabel(r"Photon flux ($\mathrm{s^{-1}}$)")

for i, n in enumerate(n_0_arr):
    plt.subplot(222)
    plt.loglog(st_m, hii_r[:, i] / pc, label=lbl_fmt.format(n))
    plt.subplot(223)
    plt.loglog(st_m, st_p[:, i], label=lbl_fmt.format(n))
    plt.subplot(224)
    plt.loglog(st_m, st_e[:, i], label=lbl_fmt.format(n))

plt.subplot(222)
plt.xlabel(r"Star mass ($\mathrm{M_\odot}$)")
plt.ylabel(r"$\mathrm{H_{II}}$ region radius ($\mathrm{pc}$)")
plt.legend(loc='best', title=r"Density ($\mathrm{cm^{-3}}$)")

plt.subplot(223)
plt.xlabel(r"Star mass ($\mathrm{M_\odot}$)")
plt.ylabel(r"$\mathrm{H_{II}}$ region momentum ($\mathrm{g\ cm / s}$)")

plt.subplot(224)
plt.xlabel(r"Star mass ($\mathrm{M_\odot}$)")
plt.ylabel(r"$\mathrm{H_{II}}$ region energy ($\mathrm{erg}$)")

plt.tight_layout()
plt.show()

