""" HII region model

Stellar flux based on a fit by O.Iffrig on data from Vacca et al. 1996
HII region model based on Geen et al. 2015

Author: Olivier Iffrig <olivier.iffrig@cea.fr>
License: GPLv3
"""

from math import pi
import numpy as np

def alpha_B_HII(T):
    """
    Recombination parameter model
    From Joki Rosdahl
    Input: T in K
    Output: HII recombination rate (in cm3 / s)
    """
    l = 315614./T
    a = 2.753e-14 * l**1.5 / (1. + (l/2.74)**0.407)**2.242
    return a

# Model parameters
stf_K  = 9.634642584812752e+48 # s**(-1)
stf_m0 = 2.728098824280431e+01 # Msun
stf_a  = 6.840015602892084e+00
stf_b  = 4.353614230584390e+00
stf_c  = 1.142166657042991e+00

def hii_fb(mass, dens_p, w, c_i, age, alpha_B, mp, T, gamma):
    """
    HII region feedback model

    Stellar flux model based on Vacca et al. 1996 data
    HII region model based on Geen et al. 2015, Eqs. (5) and (7)

    Parameters:
        mass (1D array, n_m)
            Star mass
            Unit: Msun
        dens_p (1D array, n_d)
            Density distribution parameter (n(r) = dens_p * r**(-w))
            Unit: cm**(w-3)
        w
            Density distribution exponent (see above)
        c_i
            Sound speed in ionized gas
            Unit: cm/s
        age
            HII region age
            Unit: s
        alpha_B
            Recombination parameter
            Unit: cm**3 / s
        mp
            Particle mass
            Unit: g
        T
            HII region internal "temperature" (for thermal energy)
            Value: p / rho
            Unit: cm**2 / s**2
        gamma
            Adiabatic index

    Return value (tuple)
        flux (1D array, n_m)
            Photon flux from the star
            Unit: s**(-1)
        radius (2D array, (n_m, n_d))
            HII region radius
            Unit: cm
        momentum (2D array, (n_m, n_d))
            HII region radial momentum
            Unit: g cm/s
        energy (2D array, (n_m, n_d))
            HII region energy
            Unit: erg
    """

    psi = 4. / (7. - 2. * w)
    # Source flux (index: mass) -- Fit made by O. Iffrig based on Vacca et al 
    flux = stf_K * (mass / stf_m0)**stf_a / (1.0 + (mass / stf_m0)**stf_b)**stf_c
    # HII region radius (indexes: mass, density) -- Eq. (5) from Geen et al. 2015
    radius = (3.0 / (4.0 * pi) * flux[:, np.newaxis] / alpha_B)**(psi/4.0) * (mp / dens_p[np.newaxis, :])**(psi/2.0) * (1.0 / psi * c_i * age)**psi
    # HII region momentum (indexes: mass, density) -- Eq. (7) from Geen et al. 2015
    momentum = 4.0 * pi / (3.0 - w) * dens_p[np.newaxis, :] * psi / age * radius**(4.0 - w)
    # HII region energy: kinetic part (indexes: mass, density)
    energy = 0.5 * 4.0 * pi / (3.0 - w) * dens_p[np.newaxis, :] * psi**2 / age**2 * radius**(5.0 - w)
    # HII region energy: thermal part (indexes: mass, density)
    energy += 4.0 * pi / (3.0 - w) * dens_p[np.newaxis, :] * T / (gamma - 1.0) * radius**(3.0 - w)
    return flux, radius, momentum, energy

